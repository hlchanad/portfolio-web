/**
 * Created by chanhonlun on 29/11/2017.
 */

import React from 'react';

import Aux from "../../../hoc/Aux/Aux";
import Form from "../../../forms/Form";

export default (props) => {
    return (

        <Aux>

            <div className="block-title">
                <h3>Contact Form</h3>
            </div>

            <Form formConfig={props.contactForm}
                  onSubmitForm={props.onSubmitForm}
                  onChangeControl={props.onChangeControl}
                  submitting={props.submitting}>
                <div className="messages"/>
            </Form>

        </Aux>
    );
}