/**
 * Created by chanhonlun on 29/11/2017.
 */

import React from 'react';

export default (props) => props.contacts.map(contact => {
    const value = contact.type === 'email' ? <a href={"mailto:"+contact.name}>{contact.name}</a> : contact.name;

    return (
        <div className="contact-info-block" key={contact._id}>
            <div className="ci-icon">
                <i className={"pe-7s-icon " + contact.icon}/>
            </div>
            <div className="ci-text">
                <h5>{value}</h5>
            </div>
        </div>
    )
});
