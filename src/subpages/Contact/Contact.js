/**
 * Created by chanhonlun on 28/11/2017.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import swal from 'sweetalert2';

import Subpage from "../../hoc/Subpage/Subpage";
import SubpageTitle from "../../hoc/SubpageTitle/SubpageTitle";
import SubpageContent from "../../hoc/SubpageContent/SubpageContent";
import ContactInfo from "./components/ContactInfo";
import ContactForm from "./components/ContactForm";
import * as actions from '../../store/actions/index';
import Aux from "../../hoc/Aux/Aux";
import Loading from "../../components/Loading/Loading";

class Contact extends Component {

    state = {
        contactForm: {
            formId: 'contact-form',
            controls: [
                {
                    _id: 1,
                    name: 'name',
                    type: 'input',
                    required: true,
                    elementOptions: {
                        type: 'text',
                        placeholder: 'Full Name'
                    },
                    icon: 'pe-7s-user',
                    value: ''
                },
                {
                    _id: 2,
                    name: 'email',
                    type: 'input',
                    required: true,
                    elementOptions: {
                        type: 'email',
                        placeholder: 'Email Address'
                    },
                    icon: 'pe-7s-mail',
                    value: ''
                },
                {
                    _id: 3,
                    name: 'message',
                    type: 'textarea',
                    required: true,
                    elementOptions: {
                        placeholder: 'Message for Me',
                        rows: '4'
                    },
                    icon: 'pe-7s-comment',
                    value: ''
                }
            ]
        },
        sendingContactForm: false
    };

    onChangeControlValueHandler = (_id, value) => {
        this.setState({
            contactForm: {
                ...this.state.contactForm,
                controls: this.state.contactForm.controls.map(control => {
                    if (control._id === _id) {
                        return {
                            ...control,
                            value: value
                        };
                    }
                    else {
                        return { ...control };
                    }
                })
            }
        });
    };

    onSubmitFormHandler = (value) => {
        this.setState({ sendingContactForm: true});

        axios.post('/enquiry/send', value)
            .then(response => {
                this.clearInputs();
                swal({
                    title: 'Success!',
                    text: 'Submitted successfully',
                    type: 'success',
                    timer: 3000
                });
                this.setState({
                    sendingContactForm: false
                });
            })
            .catch(error => {
                swal({
                    title: 'Error!',
                    text: 'Please try again later',
                    type: 'error'
                });
                this.setState({
                    sendingContactForm: false
                });
            });
    };

    componentWillMount() {
        this.props.onInitContact();
    }

    clearInputs = () => {
        this.setState({
            contactForm: {
                ...this.state.contactForm,
                controls: this.state.contactForm.controls.map(control => {
                    return {
                        ...control,
                        value: ''
                    }
                })
            }
        });
    };

    render() {

        const loading = this.state.sendingContactForm ? <Loading transparentBackground/> : null;

        return (
            <Aux>

                {loading}

                <Subpage pageId="contact" outerDecoration loading={this.props.initLoading}>

                    <SubpageTitle title="Contact" description="Get in Touch"/>

                    <SubpageContent>
                        <div className="row">
                            <div className="col-sm-6 col-md-6 subpage-block">
                                <div className="block-title">
                                    <h3>{this.props.info.title}</h3>
                                </div>
                                <p>{this.props.info.description}</p>

                                <ContactInfo contacts={this.props.contacts}/>
                            </div>

                            <div className="col-sm-6 col-md-6 subpage-block">
                                <ContactForm contactForm={this.state.contactForm}
                                             onSubmitForm={this.onSubmitFormHandler}
                                             onChangeControl={this.onChangeControlValueHandler}
                                             submitting={this.state.sendingContactForm}/>
                            </div>
                        </div>
                    </SubpageContent>

                </Subpage>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        initLoading: state.contact.loading,
        info: state.contact.info,
        contacts: state.contact.contacts
    }
};

const mapDispathToProps = dispatch => {
    return {
        onInitContact: () => dispatch(actions.initContact())
    }
};

export default connect(mapStateToProps, mapDispathToProps)(Contact);