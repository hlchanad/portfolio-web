/**
 * Created by chanhonlun on 28/11/2017.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import Subpage from "../../hoc/Subpage/Subpage";
import SubpageContent from "../../hoc/SubpageContent/SubpageContent";
import * as actions from '../../store/actions/index';

class Home extends Component {

    componentWillMount() {
        this.props.onInitBasic();
    }

    render() {
        let posts = this.props.posts.map(post => <p key={post} className="home-page-description">{post}</p>);
        if (this.props.posts.length > 1) {
            posts = <Slider autoplay autoplaySpeed={5000}>{posts}</Slider>;
        }

        return (
            <Subpage pageId="home"
                     disableScroll
                     extraClasses={['section-without-bg', 'section-paddings-0', 'table']}>

                <SubpageContent>
                    <div className="home-page-block">
                        <h2>{this.props.name}</h2>
                        <div className="item">
                            {posts}
                        </div>
                    </div>
                </SubpageContent>

            </Subpage>
        );
    }
}

const mapStateToProps = state => {
    return {
        initLoading: state.basic.loading,
        name: state.basic.name,
        posts: state.basic.posts
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onInitBasic: () => dispatch(actions.initBasic())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);