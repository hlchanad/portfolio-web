/**
 * Created by chanhonlun on 29/11/2017.
 */

import React from 'react';

import Aux from "../../../hoc/Aux/Aux";

export default (props) => (
    <Aux>
        <div className="block-title">
            <h3>{props.title}</h3>
        </div>
        <div className="timeline">
            {props.experiences.map(experience => (
                <div className="timeline-event te-primary" key={experience._id}>
                    <h5 className="event-date">{experience.date}</h5>
                    <h4 className="event-name">{experience.name}</h4>
                    <span className="event-description">{experience.shortDesc}</span>
                    {experience.longDescs.map(longDesc => <p key={longDesc}>{longDesc}</p>)}
                    <p>{experience.longDesc}</p>
                </div>
            ))}
        </div>
    </Aux>
);