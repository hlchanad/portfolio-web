/**
 * Created by chanhonlun on 29/11/2017.
 */

import React from 'react';

import Aux from "../../../hoc/Aux/Aux";

export default (props) => (
    <Aux>
        <div className="block-title">
            <h3>{props.title}</h3>
        </div>
        <div className="skills-info">
            {props.skills.map(skill => (
                <Aux key={skill._id}>
                    <h4>{skill.name}</h4>
                    <div className="skill-container">
                        <div className="skill-percentage" style={{ width: skill.percent + '%' }}/>
                    </div>
                </Aux>
            ))}
        </div>
    </Aux>
);