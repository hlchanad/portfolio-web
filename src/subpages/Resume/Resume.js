/**
 * Created by chanhonlun on 28/11/2017.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';

import Subpage from "../../hoc/Subpage/Subpage";
import SubpageTitle from "../../hoc/SubpageTitle/SubpageTitle";
import SubpageContent from "../../hoc/SubpageContent/SubpageContent";
import Experiences from "./components/Experiences";
// import Skills from "./components/Skills";

import * as actions from '../../store/actions/index';

class Resume extends Component {

    componentWillMount() {
        this.props.onInitResume();
    }

    render() {
        return (
            <Subpage pageId="resume" outerDecoration loading={this.props.initLoading}>

                <SubpageTitle title="Resume" description="Years of Experience"/>

                <SubpageContent>

                    <div className="row">
                        <div className="col-sm-6 col-md-4 subpage-block">
                            <Experiences title="Education & Certificates" experiences={this.props.educations}/>
                        </div>

                        <div className="col-sm-6 col-md-8 subpage-block">
                            <Experiences title="Work" experiences={this.props.works}/>
                        </div>

                        {/*<div className="col-sm-6 col-md-4 subpage-block">*/}
                        {/*    {this.props.skillSets.map(skillSet =>*/}
                        {/*        <Skills key={skillSet._id} title={skillSet.name} skills={skillSet.skills}/>)}*/}
                        {/*</div>*/}
                    </div>

                    <div className="row">
                        <div className="col-sm-12 col-md-12">
                            <div className="download-cv-block">
                                <a className="button" target="_blank" href={this.props.cvLink}>Download CV</a>
                            </div>
                        </div>
                    </div>

                </SubpageContent>

            </Subpage>
        );
    }
}

const mapStateToProps = state => {
    return {
        initLoading: state.resume.loading,
        educations: state.resume.educations,
        works: state.resume.works,
        skillSets: state.resume.skillSets,
        cvLink: state.resume.cvLink
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onInitResume: () => dispatch(actions.initResume())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Resume);