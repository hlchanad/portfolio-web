/**
 * Created by chanhonlun on 30/11/2017.
 */

import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import Subpage from "../../../hoc/Subpage/Subpage";
import SubpageContent from "../../../hoc/SubpageContent/SubpageContent";
import './PortfolioDetail.css';
import * as actions from '../../../store/actions/index';

class PortfolioDetail extends Component {

    state = {
        portfolioId: null,
        portfolioSelected: null
    };

    componentWillMount() {
        this.props.onInitPortfolio();
    }

    componentDidMount() {
        this.updatePortfolioSelected();
    }

    componentDidUpdate() {
        this.updatePortfolioSelected();
    }

    updatePortfolioSelected = () => {
        const newPortfolioId = +this.props.match.params.id;

        if (this.props.portfolios.length > 0 &&
            (!this.state.portfolioId || this.state.portfolioId !== newPortfolioId)) {

            this.setState({
                portfolioId: newPortfolioId,
                portfolioSelected: this.props.portfolios.find(portfolio => portfolio._id === newPortfolioId)
            });
        }
    };

    onNextPortfolioHandler = () => {
        if (this.state.portfolioSelected && this.state.portfolioSelected.nextId) {
            this.props.history.push('/portfolio/' + this.state.portfolioSelected.nextId);
        }
    };

    onPrevPortfolioHandler = () => {
        if (this.state.portfolioSelected && this.state.portfolioSelected.prevId) {
            this.props.history.push('/portfolio/' + this.state.portfolioSelected.prevId);
        }
    };

    onDismissPortfolioHandler = () => {
        this.props.history.push('/portfolio');
    };

    render() {
        // loading screen during fetching data
        // should error msg if portfolio not found
        if (!this.state.portfolioSelected) {
            return (
                <Subpage pageId="portfolio-not-found" loading={this.props.initLoading}>
                    <SubpageContent>portfolio not found</SubpageContent>
                </Subpage>
            );
        }
        else {
            // show portfolio detail if found

            const sliderProps = this.state.portfolioSelected.images.length <= 1 ? {} : {
                autoplay: true,
                autoplaySpeed: 5000,
                pauseOnHover: true,
                dots: true
            };
            const slider = (
                <Slider {...sliderProps}>
                    {this.state.portfolioSelected.images.map(image => (
                        <div className="item" key={image.url}>
                            <img src={image.url} alt=""/>
                        </div>
                    ))}
                </Slider>
            );

            return (
                <Subpage pageId={"portfolio-" + this.props.match.params.id}>
                    <SubpageContent>
                        <div id="ajax-page" className="ajax-page-content">
                            <div className="ajax-page-wrapper-99">
                                <div className="ajax-page-nav">
                                    <div className="nav-item ajax-page-prev-next">
                                        <a className="ajax-page-load" onClick={this.onPrevPortfolioHandler}>
                                            <i className="pe-7s-icon pe-7s-angle-left"/>
                                        </a>
                                        <a className="ajax-page-load" onClick={this.onNextPortfolioHandler}>
                                            <i className="pe-7s-icon pe-7s-angle-right"/>
                                        </a>
                                    </div>
                                    <div className="nav-item ajax-page-close-button">
                                        <a id="ajax-page-close-button"
                                           onClick={this.onDismissPortfolioHandler}>
                                            <i className="pe-7s-icon pe-7s-close"/>
                                        </a>
                                    </div>
                                </div>

                                <div className="ajax-page-title">
                                    <h1>{this.state.portfolioSelected.title}</h1>
                                </div>

                                <div className="row">
                                    <div className="col-sm-7 col-md-7 portfolio-block">
                                        <div className="portfolio-page-carousel">
                                            {slider}
                                        </div>
                                    </div>

                                    <div className="col-sm-5 col-md-5 portfolio-block">
                                        {/* Project Description */}
                                        <div className="block-title">
                                            <h3>Description</h3>
                                        </div>
                                        <ul className="project-general-info">
                                            <li><p><i className="fa fa-user"/> {this.state.portfolioSelected.author}</p></li>
                                            <li><p><i className="fa fa-globe"/> <a href={this.state.portfolioSelected.link}
                                                                                   target="_blank">{this.state.portfolioSelected.link}</a></p>
                                            </li>
                                            <li><p><i className="fa fa-calendar"/> {this.state.portfolioSelected.date}</p></li>
                                        </ul>

                                        {this.state.portfolioSelected.longDescs.map(longDesc =>
                                            <p key={longDesc} className="text-justify">{longDesc}</p>)}
                                        {/* /Project Description */}

                                        {/* Technology */}
                                        <div className="tags-block">
                                            <div className="block-title">
                                                <h3>Technology</h3>
                                            </div>
                                            <ul className="tags">
                                                {this.state.portfolioSelected.tags.map(tag => <li key={tag}><a>{tag}</a></li>)}
                                            </ul>
                                        </div>
                                        {/* /Technology */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </SubpageContent>
                </Subpage>
            );
        }
    }
}

const mapStateToProps = state => {
    return {
        initLoading: state.portfolio.loading,
        portfolios: state.portfolio.portfolios,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onInitPortfolio: () => dispatch(actions.initPortfolio())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(PortfolioDetail));