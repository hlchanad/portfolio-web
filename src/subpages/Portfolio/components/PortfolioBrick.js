/**
 * Created by chanhonlun on 30/11/2017.
 */

import React from 'react';
import './PortfolioBrick.css';

export default (props) => (
    <figure className="item PortfolioBrick" >
        <a className="ajax-page-load">
            <img src={props.thumbnail} alt=""/>
            <div onClick={props.onClickPortfolio}>
                <h5 className="name">{props.title}</h5>
                <small>{props.shortDesc}</small>
                <i className={"pe-7s-icon " + props.icon}/>
            </div>
        </a>
    </figure>
);