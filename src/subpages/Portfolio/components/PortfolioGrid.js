/**
 * Created by chanhonlun on 30/11/2017.
 */

import React from 'react';

import PortfolioBrick from "./PortfolioBrick";

export default (props) => (
    <div id="portfolio_grid" className="portfolio-grid">
        {props.portfolios.map(portfolio => (
            <PortfolioBrick key={portfolio._id}
                            onClickPortfolio={() => { props.onClickPortfolio(portfolio._id); }}
                            {...portfolio} />
        ))}
    </div>
);