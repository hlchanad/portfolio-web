/**
 * Created by chanhonlun on 28/11/2017.
 */

import React, { Component } from 'react';
import { Route, Switch, withRouter } from "react-router-dom";
import { connect } from 'react-redux';

import Aux from "../../hoc/Aux/Aux";
import Subpage from "../../hoc/Subpage/Subpage";
import SubpageTitle from "../../hoc/SubpageTitle/SubpageTitle";
import SubpageContent from "../../hoc/SubpageContent/SubpageContent";
import PortfolioGrid from "./components/PortfolioGrid";
import PortfolioDetail from "./PortfolioDetail/PortfolioDetail";
import * as actions from '../../store/actions/index';

class Portfolio extends Component {

    componentWillMount() {
        this.props.onInitPortfolio();
    }

    onClickPortfolioHandler = (_id) => {
        this.props.history.push('/portfolio/' + _id);
    };

    render() {
        const portfolioGrid = (
            <Subpage pageId="portfolio" outerDecoration loading={this.props.initLoading}>
                <SubpageTitle title="Portfolio" description="My Best Works"/>

                <SubpageContent>
                    <div className="portfolio-content">
                        <PortfolioGrid portfolios={this.props.portfolios}
                                       onClickPortfolio={this.onClickPortfolioHandler}/>
                    </div>
                </SubpageContent>
            </Subpage>
        );

        return (
            <Aux>
                <Switch>
                    <Route exact path="/portfolio" render={() => portfolioGrid}/>
                    <Route exact path="/portfolio/:id" component={PortfolioDetail}/>
                </Switch>
            </Aux>
        )
    }
}

const mapStateToProps = state => {
    return {
        initLoading: state.portfolio.loading,
        portfolios: state.portfolio.portfolios,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onInitPortfolio: () => dispatch(actions.initPortfolio())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Portfolio));