/**
 * Created by chanhonlun on 28/11/2017.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';

import MainPhoto from "../../components/MainPhoto/MainPhoto";
import Subpage from "../../hoc/Subpage/Subpage";
import SubpageTitle from "../../hoc/SubpageTitle/SubpageTitle";
import SubpageContent from "../../hoc/SubpageContent/SubpageContent";
import SocialLinks from "./components/SocialLinks";
import PersonalInfo from "./components/PersonalInfo";
import BasicDescription from "./components/BasicDescription";
import * as actions from '../../store/actions/index';

class AboutMe extends Component {

    componentWillMount() {
        this.props.onInitAboutMe();
    }

    render() {
        return (
            <Subpage pageId="about_me" outerDecoration loading={this.props.initLoading}>

                <SubpageTitle title="About Me" description="Thinker, Creative Doer"/>

                <SubpageContent>

                    <div className="row">
                        <div className="col-sm-6 col-md-6 col-lg-4 subpage-block" style={{textAlign: 'center'}}>
                            <MainPhoto extraClasses={['my-photo-block']}/>
                        </div>

                        <div className="col-sm-6 col-md-6 col-lg-4">
                            <BasicDescription basicDesc={this.props.basicDesc}/>
                        </div>

                        <div className="col-sm-6 col-md-6 col-lg-4 subpage-block">
                            <PersonalInfo personalInfo={this.props.personalInfo}/>
                            <SocialLinks socialLinks={this.props.socialLinks}/>
                        </div>
                    </div>

                </SubpageContent>

            </Subpage>
        );
    }
}

const mapStateToProps = state => {
    return {
        initLoading: state.aboutMe.loading,
        socialLinks: state.aboutMe.socialLinks,
        personalInfo: state.aboutMe.personalInfo,
        basicDesc: state.aboutMe.basicDesc
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onInitAboutMe: () => dispatch(actions.initAboutMe())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AboutMe);