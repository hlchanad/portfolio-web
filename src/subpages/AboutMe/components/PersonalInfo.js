/**
 * Created by chanhonlun on 28/11/2017.
 */

import React from 'react';

export default (props) => (
    <ul className="info-list">
        {props.personalInfo.map(info => {
            const value = info.type === 'email' ? <a href={"mailto:"+info.value}>{info.value}</a> : info.value;
            return (
                <li key={info._id}>
                    <span className="title">{info.title}</span>
                    <span className="value">{value}</span>
                </li>
            );
        })}
    </ul>
);