/**
 * Created by chanhonlun on 28/11/2017.
 */

import React from 'react';

export default (props) => (
    <ul className="social-links">
        {/* Full list of social icons: http://fontawesome.io/icons/#brand */}
        {props.socialLinks.map(socialLink => (
            <li key={socialLink._id}>
                <a className="top social-button"
                   href={socialLink.link}
                   title={socialLink.title}
                   target="_blank">
                    <i className={socialLink.icon}/>
                </a>
            </li>
        ))}
    </ul>
);