/**
 * Created by chanhonlun on 28/11/2017.
 */

import React from 'react';

import Aux from "../../../hoc/Aux/Aux";

export default (props) => (
    <Aux>
        <h3>{props.basicDesc.title}</h3>
        {props.basicDesc.descriptions.map((desc, index) => <p key={index}>{desc}</p>)}
    </Aux>
);