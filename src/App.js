import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import { config } from "./helpers";

import './App.css';
import Aux from "./hoc/Aux/Aux";
import Loading from "./components/Loading/Loading";
import Navigation from "./Navigation/Navigation";
import mainBgLight from './assets/unique-theme/images/main_bg_light.jpg';

import Home from "./subpages/Home/Home";
import AboutMe from "./subpages/AboutMe/AboutMe";
import Resume from "./subpages/Resume/Resume";
import Portfolio from "./subpages/Portfolio/Portfolio";
import Contact from "./subpages/Contact/Contact";

class App extends Component {

    state = {
        websiteTitle: config.get('app.websiteTitle'),
        showLoading: true
    };

    componentDidMount() {
        this.setState({showLoading: false});
        document.title = this.state.websiteTitle;
    }

    render() {
        return (
            <Aux>
                {this.state.showLoading ? <Loading /> : null}

                <div id="page" className="page">
                    <Navigation />

                    <div id="main" className="site-main">
                        <div className="pt-wrapper" style={{backgroundImage: 'url(' + mainBgLight + ')'}}>
                            <div className="subpages">

                                <Switch>
                                    <Route exact path="/home" component={Home}/>
                                    <Route exact path="/about-me" component={AboutMe}/>
                                    <Route exact path="/resume" component={Resume}/>
                                    <Route path="/portfolio" component={Portfolio}/>
                                    <Route exact path="/contact" component={Contact}/>
                                    <Redirect to="/home"/>
                                </Switch>

                            </div>
                        </div>
                    </div>

                </div>
            </Aux>
        );
    }
}

export default App;
