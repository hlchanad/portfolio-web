/**
 * Created by chanhonlun on 28/11/2017.
 */

import React, { Component } from 'react';

import { Scrollbars } from 'react-custom-scrollbars';

import Aux from "../Aux/Aux";
import Loading from "../../components/Loading/Loading";

export default class extends Component {

    render() {

        const classes = ['pt-page', 'pt-page-current'];

        if (this.props.extraClasses && Array.isArray(this.props.extraClasses) && this.props.extraClasses.length > 0) {
            classes.push(...this.props.extraClasses);
        }

        let content = <Loading />;

        if (!this.props.loading) {
            const outerDecoration = this.props.outerDecoration ? <div className="border-block-top-110"/> : null;

            content = (
                <Scrollbars style={{ width: '100%', height: '100%' }}>
                    {outerDecoration}
                    <div className="section-inner">
                        {this.props.children}
                    </div>
                </Scrollbars>
            );
            if (this.props.disableScroll) {
                content = (
                    <Aux>
                        {outerDecoration}
                        <div className="section-inner">
                            {this.props.children}
                        </div>
                    </Aux>
                );
            }
        }

        return (
            <section className={classes.join(' ')}>
                {content}
            </section>
        );
    }
}