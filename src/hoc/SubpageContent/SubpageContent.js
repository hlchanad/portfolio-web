/**
 * Created by chanhonlun on 28/11/2017.
 */

import React from 'react';
import Aux from "../Aux/Aux";

export default (props) => (
    <Aux>{props.children}</Aux>
);