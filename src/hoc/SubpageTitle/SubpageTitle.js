/**
 * Created by chanhonlun on 28/11/2017.
 */

import React from 'react';

export default (props) => (
    <div className="section-title-block">
        <div className="section-title-wrapper clearfix">
            <h2 className="section-title">{props.title}</h2>
            <h5 className="section-description">{props.description}</h5>
        </div>
    </div>
);