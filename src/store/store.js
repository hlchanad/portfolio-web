/**
 * Created by chanhonlun on 1/12/2017.
 */

import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import thunk from 'redux-thunk';

import basicReducer from './reducers/basic';
import aboutMeReducer from './reducers/aboutMe';
import resumeReducer from './reducers/resume';
import portfolioReducer from './reducers/portfolio';
import contactReducer from './reducers/contact';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE || compose;

const rootReducer = combineReducers({
    basic: basicReducer,
    aboutMe: aboutMeReducer,
    resume: resumeReducer,
    portfolio: portfolioReducer,
    contact: contactReducer
});

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));

export default store;