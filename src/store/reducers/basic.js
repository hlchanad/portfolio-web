/**
 * Created by chanhonlun on 1/12/2017.
 */

import * as actionTypes from '../actions/actionTypes';

const initialState = {
    loading: false,
    loaded: false,
    name: '',
    posts: [],
    mainPhoto: null
};

const initBasic = (state, action) => {
    return {
        ...state,
        loading: true
    };
};

const finishInitBasic = (state, action) => {
    return {
        ...state,
        loading: false,
        loaded: true,
        name: action.payload.name,
        posts: action.payload.posts,
        mainPhoto: action.payload.mainPhoto
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.INIT_BASIC: return initBasic(state, action);
        case actionTypes.FINISH_INIT_BASIC: return finishInitBasic(state, action);
        default: return state;
    }
};