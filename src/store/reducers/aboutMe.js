/**
 * Created by chanhonlun on 1/12/2017.
 */

import * as actionTypes from '../actions/actionTypes';

const initialState = {
    loading: false,
    loaded: false,
    socialLinks: [],
    personalInfo: [],
    basicDesc: {
        title: '',
        descriptions: []
    }
};

const initAboutMe = (state, action) => {
    return {
        ...state,
        loading: true
    };
};

const finishInitAboutMe = (state, action) => {
    return {
        ...state,
        loading: false,
        loaded: true,
        socialLinks: action.payload.socialLinks,
        personalInfo: action.payload.personalInfo,
        basicDesc: action.payload.basicDesc
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.INIT_ABOUT_ME: return initAboutMe(state, action);
        case actionTypes.FINISH_INIT_ABOUT_ME: return finishInitAboutMe(state, action);
        default: return state;
    }
};