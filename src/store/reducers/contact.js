/**
 * Created by chanhonlun on 1/12/2017.
 */

import * as actionTypes from '../actions/actionTypes';

const initialState = {
    loading: false,
    loaded: false,
    info: { title: '', description: ''},
    contacts: []
};

const initContact = (state, action) => {
    return {
        ...state,
        loading: true
    };
};

const finishInitContact = (state, action) => {
    return {
        ...state,
        loading: false,
        loaded: true,
        info: action.payload.info,
        contacts: action.payload.contacts
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.INIT_CONTACT: return initContact(state, action);
        case actionTypes.FINISH_INIT_CONTACT: return finishInitContact(state, action);
        default: return state;
    }
};