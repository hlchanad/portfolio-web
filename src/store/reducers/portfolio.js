/**
 * Created by chanhonlun on 1/12/2017.
 */

import * as actionTypes from '../actions/actionTypes';

const initialState = {
    loading: false,
    loaded: false,
    portfolios: []
};

const initPortfolio = (state, action) => {
    return {
        ...state,
        loading: true
    };
};

const finishInitPortfolio = (state, action) => {
    return {
        ...state,
        loading: false,
        loaded: true,
        portfolios: action.payload.portfolios
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.INIT_PORTFOLIO: return initPortfolio(state, action);
        case actionTypes.FINISH_INIT_PORTFOLIO: return finishInitPortfolio(state, action);
        default: return state;
    }
};