/**
 * Created by chanhonlun on 1/12/2017.
 */

import * as actionTypes from '../actions/actionTypes';

const initialState = {
    loading: false,
    loaded: false,
    educations: [],
    works: [],
    skillSets: [],
    cvLink: ''
};

const initResume = (state, action) => {
    return {
        ...state,
        loading: true
    };
};

const finishInitResume = (state, action) => {
    return {
        ...state,
        loading: false,
        loaded: true,
        educations: action.payload.educations,
        works: action.payload.works,
        skillSets: action.payload.skillSets,
        cvLink: action.payload.cvLink
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.INIT_RESUME: return initResume(state, action);
        case actionTypes.FINISH_INIT_RESUME: return finishInitResume(state, action);
        default: return state;
    }
};