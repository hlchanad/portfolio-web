/**
 * Created by chanhonlun on 1/12/2017.
 */

import store from '../store';
import * as actionTypes from './actionTypes';
import axiosData from '../../http/axios-data';

const finishInitPortfolio = (portfolios) => {
    return {
        type: actionTypes.FINISH_INIT_PORTFOLIO,
        payload: {
            portfolios: portfolios
        }
    };
};

const loadingInitPortfolio = () => {
    return {
        type: actionTypes.INIT_PORTFOLIO
    };
};

export const initPortfolio = () => {
    const portfolioState = store.getState().portfolio;

    if (portfolioState.loaded) {
        return finishInitPortfolio(portfolioState.portfolios);
    }
    else {
        return async dispatch => {
            dispatch(loadingInitPortfolio());

            // axios to get from server or load json locally instead
            const response = (await axiosData.get('/portfolio.json')).data;
            dispatch(finishInitPortfolio(response.portfolios));
        }
    }
};