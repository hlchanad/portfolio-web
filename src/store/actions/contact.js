/**
 * Created by chanhonlun on 1/12/2017.
 */

import store from '../store';
import * as actionTypes from './actionTypes';
import axiosData from "../../http/axios-data";

const finishInitContact = (info, contacts) => {
    return {
        type: actionTypes.FINISH_INIT_CONTACT,
        payload: {
            info: info,
            contacts: contacts
        }
    };
};

const loadingInitContact = () => {
    return {
        type: actionTypes.INIT_CONTACT
    };
};

export const initContact = () => {
    const contactState = store.getState().contact;

    if (contactState.loaded) {
        return finishInitContact(contactState.info, contactState.contacts);
    }
    else {
        return async dispatch => {
            dispatch(loadingInitContact());

            // axios to get from server or load json locally instead
            const response = (await axiosData.get('/contact.json')).data;
            dispatch(finishInitContact(response.info, response.contacts));
        }
    }

};