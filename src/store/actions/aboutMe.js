/**
 * Created by chanhonlun on 1/12/2017.
 */

import store from '../store';
import * as actionTypes from './actionTypes';
import axiosData from "../../http/axios-data";

const finishInitAboutMe = (socialLinks, personalInfo, basicDesc) => {
    return {
        type: actionTypes.FINISH_INIT_ABOUT_ME,
        payload: {
            socialLinks: socialLinks,
            personalInfo: personalInfo,
            basicDesc: basicDesc
        }
    };
};

const loadingInitAboutMe = () => {
    return {
        type: actionTypes.INIT_ABOUT_ME
    };
};

export const initAboutMe = () => {
    const aboutMeState = store.getState().aboutMe;

    if (aboutMeState.loaded) {
        return finishInitAboutMe(aboutMeState.socialLinks, aboutMeState.personalInfo, aboutMeState.basicDesc)
    }
    else {
        return async dispatch => {
            dispatch(loadingInitAboutMe());

            // axios to get from server or load json locally instead
            const response = (await axiosData.get('/aboutMe.json')).data;
            dispatch(finishInitAboutMe(response.socialLinks, response.personalInfo, response.basicDesc));
        }
    }

};