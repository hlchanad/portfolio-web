/**
 * Created by chanhonlun on 1/12/2017.
 */

import store from '../store';
import * as actionTypes from './actionTypes';
import axiosData from "../../http/axios-data";

const finishInitResume = (educations, works, skillSets, cvLink) => {
    return {
        type: actionTypes.FINISH_INIT_RESUME,
        payload: {
            educations: educations,
            works: works,
            skillSets: skillSets,
            cvLink: cvLink
        }
    };
};

const loadingInitResume = () => {
    return {
        type: actionTypes.INIT_RESUME
    };
};

export const initResume = () => {
    const resumeState = store.getState().resume;

    if (resumeState.loaded) {
        return finishInitResume(resumeState.educations, resumeState.works, resumeState.skillSets, resumeState.cvLink);
    }
    else {
        return async dispatch => {
            dispatch(loadingInitResume());

            // axios to get from server or load json locally instead
            const response = (await axiosData.get('/resume.json')).data;
            dispatch(finishInitResume(response.educations, response.works, response.skillSets, response.cvLink));
        }
    }
};