/**
 * Created by chanhonlun on 1/12/2017.
 */

import store from '../store';
import * as actionTypes from './actionTypes';
import axiosData from "../../http/axios-data";

const finishInitBasic = (name, posts, mainPhoto) => {
    return {
        type: actionTypes.FINISH_INIT_BASIC,
        payload: {
            name: name,
            posts: posts,
            mainPhoto: mainPhoto
        }
    };
};

const loadingInitBasic = () => {
    return {
        type: actionTypes.INIT_BASIC
    };
};

export const initBasic = () => {
    const basicState = store.getState().basic;

    if (basicState.loaded) {
        return finishInitBasic(basicState.name, basicState.posts, basicState.mainPhoto)
    }
    else {
        return async dispatch => {
            dispatch(loadingInitBasic());

            // axios to get from server or load json locally instead
            const response = (await axiosData.get('/basic.json')).data;
            dispatch(finishInitBasic(response.name, response.posts, response.mainPhoto));
        }
    }

};