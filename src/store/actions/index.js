/**
 * Created by chanhonlun on 1/12/2017.
 */

export { initBasic } from './basic';

export { initAboutMe } from './aboutMe';

export { initResume } from './resume';

export { initContact } from './contact';

export { initPortfolio } from './portfolio';