import _ from 'lodash'

let _configs

export function apply(configs) {
    _configs = configs
}

export function get(path, defaultValue) {
    return _.get(_configs, path, defaultValue)
}