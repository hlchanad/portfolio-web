/**
 * Created by chanhonlun on 28/11/2017.
 */

import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';

export default withRouter(class extends React.Component {

    render() {
        let isActive = false;
        if ((this.props.exact && this.props.location.pathname === this.props.link)
            || (!this.props.exact && this.props.location.pathname.indexOf(this.props.link) >= 0)) {
            isActive = true;
        }

        return (
            <li className={isActive ? 'active' : ''} onClick={this.props.onClickMenuItem}>
                <NavLink to={this.props.link} exact={this.props.exact}>
                    <i className={"menu-icon pe-7s-icon " + this.props.icon}/>
                    <span>{this.props.title}</span>
                </NavLink>
            </li>
        );
    }
});