/**
 * Created by chanhonlun on 28/11/2017.
 */

import React from 'react';

import MainPhoto from "../../components/MainPhoto/MainPhoto";
import HeaderLink from "./HeaderLink/HeaderLink";
import './Header.css';

export default (props) => (
    <header id="site_header" className="header mobile-menu-hide header-color-light">
        <MainPhoto extraClasses={['my-photo']}/>

        <div className="site-title-block">
            <h1 className="site-title">{props.name}</h1>
        </div>

        <div className="site-nav">
            <ul id="nav" className="site-main-menu">
                {props.links.map(link =>
                    <HeaderLink {...link} key={link._id} onClickMenuItem={props.onClickMenuItem}/>)}
            </ul>
        </div>
    </header>
);