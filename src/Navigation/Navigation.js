/**
 * Created by chanhonlun on 28/11/2017.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';

import Aux from "../hoc/Aux/Aux";
import Header from "./Header/Header";
import MobileHeader from "./MobileHeader/MobileHeader";
import * as actions from '../store/actions/index';

class Navigation extends Component {

    state = {
        links: [
            {
                _id: 1,
                link: '/home',
                icon: 'pe-7s-home',
                title: 'Home',
                exact: true
            },
            {
                _id: 2,
                link: '/about-me',
                icon: 'pe-7s-user',
                title: 'About Me',
                exact: true
            },
            {
                _id: 3,
                link: '/resume',
                icon: 'pe-7s-id',
                title: 'Resume',
                exact: true
            },
            {
                _id: 4,
                link: '/portfolio',
                icon: 'pe-7s-portfolio',
                title: 'Portfolio',
                exact: false
            },
            {
                _id: 5,
                link: '/contact',
                icon: 'pe-7s-mail',
                title: 'Contact',
                exact: true
            }
        ]
    };

    toggleMenuHandler = () => {
        const header = document.querySelector('header#site_header');

        if (header.classList.contains('mobile-menu-hide')) {
            header.classList.remove('mobile-menu-hide');
        }
        else {
            header.classList.add('mobile-menu-hide');
        }
    };

    render() {
        return (
            <Aux>
                <Header name={this.props.name} links={this.state.links} onClickMenuItem={this.toggleMenuHandler}/>
                <MobileHeader name={this.props.name} onClickMenuToggle={this.toggleMenuHandler}/>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        name: state.basic.name
    };

};

const mapDispatchToProps = dispatch => {
    return {
        onInitBasic: () => dispatch(actions.initBasic())
    };
};
export default connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(Navigation);