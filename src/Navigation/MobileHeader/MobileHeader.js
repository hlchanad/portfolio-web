/**
 * Created by chanhonlun on 28/11/2017.
 */

import React from 'react';

export default (props) => (
    <div className="mobile-header mobile-visible">
        <div className="mobile-logo-container">
            <div className="mobile-site-title">{props.name}</div>
        </div>

        <a className="menu-toggle mobile-visible" onClick={props.onClickMenuToggle}>
            <i className="fa fa-bars"/>
        </a>
    </div>
);