import * as configs from './config'
import { config } from './helpers'

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import axios from 'axios';

import './unique-theme-imports'; // Unique theme css

import App from './App';
import registerServiceWorker from './registerServiceWorker';
import store from "./store/store";
import axiosData from './http/axios-data'

config.apply(configs);

axios.defaults.baseURL = config.get('app.serverUrl');
axiosData.defaults.baseURL = config.get('app.dataUrl');

const app = (
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
