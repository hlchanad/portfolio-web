export const app = {
    websiteTitle: 'Hon Lun, CHAN',
    dataUrl: 'https://hlchanad-portfolio.s3-ap-southeast-1.amazonaws.com/data',
    serverUrl: 'https://portfolio-api.chanhonlun.com'
}
