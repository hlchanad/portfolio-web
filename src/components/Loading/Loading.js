/**
 * Created by chanhonlun on 28/11/2017.
 */

import React from 'react';
import './Loading.css';

export default (props) => (
    <div className={"preloader " + (props.transparentBackground ? 'transparent-70' : '')}>
        <div className="preloader-animation">
            <div className="dot1"/>
            <div className="dot2"/>
        </div>
    </div>
);
