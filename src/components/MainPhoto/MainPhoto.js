/**
 * Created by chanhonlun on 28/11/2017.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Tilt from 'react-tilt';

import './MainPhoto.css';
import * as actions from '../../store/actions/index';

class MainPhoto extends Component {

    componentWillMount() {
        this.props.onInitBasic();
    }

    render() {
        const classes = [ 'Tilt' ];

        if (this.props.extraClasses && Array.isArray(this.props.extraClasses) && this.props.extraClasses.length > 0) {
            classes.push(...this.props.extraClasses);
        }

        return (
            <Tilt className={classes.join(" ")} options={{ max: 20 }}>
                <div className="Tilt-inner">
                    {this.props.mainPhoto && <img className="MainPhoto" src={this.props.mainPhoto.url} alt="face"/>}
                </div>
            </Tilt>
        );
    }
}

const mapStateToProps = state => {
    return {
        mainPhoto: state.basic.mainPhoto
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onInitBasic: () => dispatch(actions.initBasic())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPhoto);