/**
 * Created by chanhonlun on 29/11/2017.
 */

import React, { Component } from 'react';

import Input from "./inputs/Input";
import Textarea from "./inputs/Textarea";

export default class extends Component {

    state = {
        error: false,
        submitButtonText: 'Submit',
        submittingButtonText: 'Submitting ...',
    };

    formValue = () => {
        const value = {};
        this.props.formConfig.controls.forEach(control => {
            value[control.name] = control.value
        });
        return value;
    };

    onChangeControlHandler = (event, _id) => {

        this.props.onChangeControl(_id, event.target.value);

        // this.setState({
        //     controls: this.updateControlsProps(this.state.controls, _id, {
        //         value: event.target.value,
        //         touched: true
        //     })
        // });
    };

    onSubmitHandler = (event) => {
        event.preventDefault();

        if (!this.state.error) {
            this.props.onSubmitForm(this.formValue());

            // this.setState({
            //     controls: this.state.controls.map(control => {
            //         return {
            //             ...control,
            //             value: ''
            //         };
            //     })
            // });
        }
    };

    renderControls = () => {
        return this.props.formConfig.controls.map(control => {
            switch (control.type) {
                case 'input':
                    return <Input key={control._id}
                                  onChange={this.onChangeControlHandler}
                                  {...control}/>;

                case 'textarea':
                    return <Textarea key={control._id}
                                     onChange={this.onChangeControlHandler}
                                     {...control}/>;

                default:
                    return null;
            }
        });
    };

    render() {
        return (
            <form id={this.props.formConfig.formId} onSubmit={this.onSubmitHandler}>
                {this.props.children}

                <div className="controls">
                    {this.renderControls()}
                </div>

                <input type="submit"
                       className="button btn-send"
                       value={this.props.submitting ? this.state.submittingButtonText : this.state.submitButtonText}/>
            </form>
        );
    }
}