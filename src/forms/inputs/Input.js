/**
 * Created by chanhonlun on 29/11/2017.
 */

import React from 'react';

export default (props) => (
    <div className="form-group">
        <input id={props.name}
               name={props.name}
               className="form-control"
               onChange={event => props.onChange(event, props._id)}
               required={props.required}
               value={props.value}
               {...props.elementOptions}/>
        <div className="form-control-border"/>
        <i className={"form-control-icon " + props.icon}/>
        <div className="help-block with-errors"/>
    </div>
);